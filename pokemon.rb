module Damaging
  def inflict_damage_on(opponent)
    opponent.hp -= damage_amount(opponent) unless opponent.hp == 0
  end

  def damage_amount(opponent)
    strength * DamageChart.new(attacker: type, attackee: opponent.type).damage_multiplier
  end
end

class DamageChart
  def initialize(attacker:, attackee:)
    @attacker = attacker
    @attackee = attackee
  end

  def damage_multiplier
    chart[@attacker][@attackee]
  end
  private 

  def chart
    {
      grass: { grass: 1, fire: 1, water: 2},
      water: { grass: 1, fire: 2, water: 1},
      fire:  { grass: 2, fire: 1, water: 1},
    }
  end
end

class Pokemon
  include Damaging

  def initialize
    @hp       = 100
    @strength = 10
  end

  attr_accessor :hp
  attr_reader   :strength

  def attack(opponent)
    inflict_damage_on(opponent) 
  end
end

class Squirtle < Pokemon
  def type
    :water
  end
end

class Bulbasaur < Pokemon
  def type
    :grass
  end
end

class Charmander < Pokemon
  def type
    :fire
  end
end
